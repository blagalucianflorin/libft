#include	"libft.h"

size_t	ft_strlcat(char *d, const char *s, size_t n)
{
	size_t	sleft;

	sleft = n;
	while(*d && sleft-- > 0)
		d++;
	while(*s && sleft > 1 && sleft--)
		*d++ = *s++;
	if(sleft == 1)
		*d = '\0';
	return (n - sleft);
}
