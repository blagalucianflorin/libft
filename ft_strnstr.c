#include	"libft.h"

char	*ft_strnstr(const char *box, const char *item, size_t n)
{
	char	*start;
	char	*init;

	init = (char *)item;
	while(*box && n--)
	{
		item = init;
		start = (char *)box;
		if(*box == *item)
			while(*box == *item && n--)
			{
				box++;
				item++;
				if(!(*item))
					return (start);
			}
		box++;
	}
	return (NULL);
}
