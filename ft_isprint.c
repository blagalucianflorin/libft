#include	"libft.h"

int	ft_isprint(int c)
{
	return (32 <= c && 127 >= c);
}
