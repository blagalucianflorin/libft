#include	"libft.h"

void	*ft_memmove(void *d, const void *s, size_t n)
{
	unsigned char	*ptr;

	ptr = (unsigned char *)d;
	if(d > s)
	{
		s += n - 1;
		d += n - 1;
		while(n--)
			*(unsigned char *)d-- = *(unsigned char *)s--;
		return (ptr);
	}
	else
		while(n--)
			*(unsigned char *)d++ = *(unsigned char *)s++;
	return (ptr);
}
