#include	"libft.h"

int	isascii(int c)
{
	return (0 <= c && 127 >= c);
}
