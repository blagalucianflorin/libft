/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: blucian <blaga.lucianflorin@gmail.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 14:35:48 by blucian           #+#    #+#             */
/*   Updated: 2016/08/29 14:46:55 by blucian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"

void	*ft_memalloc(size_t size)
{
	void	*ret;

	if ((ret = malloc(size * sizeof(unsigned char))) == NULL)
		return (NULL);
	ft_memset(ret, ' ', size);
	return	(ret);
}
