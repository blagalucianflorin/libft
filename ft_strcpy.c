#include	"libft.h"

char	*ft_strcpy(char *d, const char *s)
{
	char	*ptr;

	ptr = d;
	while(*ptr)
		*d++ = *s++;
	*d = '\0';
	return (ptr);
}
