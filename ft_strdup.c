#include	"libft.h"

char	*ft_strdup(const char *s)
{
	char	*d;

	d = malloc(ft_strlen(s) * sizeof(char));
	ft_memcpy(d, s, ft_strlen(s));
	return (d);
}
