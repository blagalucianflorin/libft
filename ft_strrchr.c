#include	"libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	firstch;

	firstch = *s;
	s += ft_strlen(s) - 1;
	while(*s != firstch)
		if(*s == (char)c)
			return ((char *)s);
		else
			s++;
	return (NULL);
}
