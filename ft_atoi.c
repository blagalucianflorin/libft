#include	"libft.h"

int	ft_atoi(const char *s)
{
	int	output;
	int	sign;

	output = 0;
	sign = 1;
	while (*s == ' ' || *s == '\t' || *s == '\n')
		s++;
	if(*s == '-')
	{
		sign = -1;
		s++;
	}
	else
		if (*s == '+')
			s++;
	while (*s >= '0' && *s <= '9')
		output = output * 10 + (*s++ - '0') * sign;
	return (output);
}
