/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: blucian <blaga.lucianflorin@gmail.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 16:47:38 by blucian           #+#    #+#             */
/*   Updated: 2016/09/01 17:02:56 by blucian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"

char	*ft_strmap(char const *s, char (*f)(unsigned int, char))
{
	char			*ret;
	unsigned int	i;

	i = -1;
	ret = malloc((ft_strlen(s) + 1) * sizeof(char));
	if(ret)
		while(s[++i])
			ret[i] = f(i, s[i]);
	ret[++i] = '\0';
	return (ret);
}
