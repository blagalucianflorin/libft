/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: blucian <blaga.lucianflorin@gmail.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 17:16:34 by blucian           #+#    #+#             */
/*   Updated: 2016/09/01 17:20:53 by blucian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*ret;

	ret = malloc(len * sizeof(char));
	if(ret)
	{
		s += start - 1;
		while(len--)
			*ret++ = *s++;
		*ret = '\0';
		return (ret);
	}
	return (NULL);
}
