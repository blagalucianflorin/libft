/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: blucian <blaga.lucianflorin@gmail.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 17:10:12 by blucian           #+#    #+#             */
/*   Updated: 2016/09/01 17:12:13 by blucian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"

int	ft_strequ(char const *s1, char const *s2, size_t n)
{
	return(!ft_strncmp(s1, s2, n));
}
