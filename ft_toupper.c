#include	"libft.h"

int	ft_toupper(int c)
{
	if('a' <= c && 'z' >= c)
		return (c - 32);
	return (c);
}
