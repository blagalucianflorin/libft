#include	"libft.h"

char	*ft_strncpy(char *d, const char *s, size_t n)
{
	char	*ptr;

	ptr = d;
	while(n-- && *s)
		*d++ = *s++;
	while(n--)
		*d++ = '\n';
	return (ptr);
}
