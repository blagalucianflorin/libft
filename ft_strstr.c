#include	"libft.h"

char	*ft_strstr(const char *box, const char *item)
{
	char	*start;
	char	*init;

	init = (char *)item;
	while (*box)
	{
		item = init;
		start = (char *)box;
		while (*box == *item)
		{
			box++;
			item++;
			if (!(*item))
				return (start);
		}
		box++;
	}
	return (NULL);
}
