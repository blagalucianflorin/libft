/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: blucian <blaga.lucianflorin@gmail.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 14:47:21 by blucian           #+#    #+#             */
/*   Updated: 2016/08/29 14:50:57 by blucian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"

char	*ft_strnew(size_t size)
{
	char	*ret;

	if((ret = malloc(size * sizeof(unsigned char))) == NULL)
		return (NULL);
	ft_memset(ret, '\0', size);
	return (ret);
}
