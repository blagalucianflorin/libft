#include	"libft.h"

void	*ft_memcpy(void *d, const void *s, size_t n)
{
	void	*ptrd;

	ptrd = d;
	while(n--)
		*(unsigned char *)ptrd = *(unsigned char *)s;
	return (ptrd);
}
