#include	"libft.h"

int	ft_tolower(int c)
{
	if('A' <= c && 'Z' >= c)
		return (c + 32);
	return (c);
}
