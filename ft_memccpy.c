#include	"libft.h"

void	*ft_memccpy(void *d, const void *s, int c, size_t n)
{
	unsigned char	*ptr;

	ptr = (unsigned char *)d;
	while(n--)
		if((*(unsigned char *)ptr++ = *(unsigned char *)s++) == c)
			return (ptr);
	return (NULL);
}
