/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: blucian <blaga.lucianflorin@gmail.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 17:29:57 by blucian           #+#    #+#             */
/*   Updated: 2016/09/01 17:36:34 by blucian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"

char	*ft_strtrim(char const *s)
{
	char	*ret;
	char	*aux;
	char	*aux2;

	aux = (char *)s;
	while(*s == ' ' || *s == '\n' || *s == '\t')
		s++;
	aux2 = (char *)s;
	s += ft_strlen(s);
	while(*s == ' ' || *s == '\n' || *s == '\t')
		s--;
	aux = (char *)s;
	ret = malloc((aux2 - aux) * sizeof(char));
	ft_strncpy(ret, aux, (size_t)aux2 - (size_t)aux);
	return (ret);
}
