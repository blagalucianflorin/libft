#include	"libft.h"

char	*ft_strncat(char *d, const char *s, size_t n)
{
	char	*ptr;

	ptr = d;
	d += ft_strlen(d);
	while(n-- && *s)
		*d++ = *s++;
	*d = '\0';
	return (ptr);
}
