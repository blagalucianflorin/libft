#include	"libft.h"

void	*memset(void *s, int c, size_t n)
{
	unsigned char *ptr;

	ptr = (unsigned char *)s;
	while(n--)
		*ptr++ = (unsigned char)c;
	return (s);
}
